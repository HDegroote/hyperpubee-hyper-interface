# Hyperpubee Hyper Interface

Small interface on top of hypercore to facilitate creating
and reading hypercores and hyperbees in a single corestore.

## Install
`npm add hyperpubee-hyper-interface`

## Usage

Note: this example uses random-access-memory (needs to be installed).

```
const Corestore = require('corestore')
const ram = require('random-access-memory')
const HyperInterface = require('hyperpubee-hyper-interface')

const corestore = new Corestore(ram)

// opts passed to every bee created/read through this interface (by default uses hyperbee's defaults)
const defaultBeeOpts = { keyEncoding: 'utf-8', valueEncoding: 'utf-8' }

const hyperInterface = new HyperInterface(corestore, { defaultBeeOpts })
await hyperInterface.ready()

const bee = await hyperInterface.createBee('testbee')
await bee.put('myKey', 'myValue')

const readBee = await hyperInterface.readBee(bee.feed.key)
const { value } = await readBee.get('myKey')
console.log(value) // 'myValue'

// Or getting the entry directly (optional version param)
const entry = await hyperInterface.getEntry({hash: bee.feed.key, location: 'myKey'})
console.log(entry) // 'myValue'

```

Equivalent for hypercore with `hyperInterface.readCore` and `hyperInterface.createCore`
