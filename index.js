const Hyperbee = require('hyperbee')
const { asHex, asBuffer } = require('hexkey-utils')
const safetyCatch = require('safety-catch')

async function ensureIsNonExistingCoreName (name, corestore) {
  try {
    await (corestore.get({ name, createIfMissing: false })).ready()
  } catch (e) {
    safetyCatch(e)
    return // Error confirms it is indeed missing
  }
  throw new Error(`A hypercore with name '${name}' already exists in the corestore`)
}

class HyperInterface {
  constructor (corestore, { defaultBeeOpts = {} } = {}) {
    if (!corestore) throw new Error('Must provide a corestore')
    this.corestore = corestore

    // default core opts can be passed directly to the corestore
    this.defaultBeeOpts = { ...defaultBeeOpts }
  }

  async createCore (name, opts) {
    // Note: this method guarantees that it will not create a hypercore with
    // the same name twice only if it is awaited individually.
    // It will 'create' the same hypercore twice (e.g. return 2 copies)
    // if called with an equivalent of await Promise.all(
    //   hyperInterface.createCore('core1'), hyperInterface.createCore('core1')
    // )
    if (!name) {
      throw new Error('Must provide a name when creating a hypercore')
    }
    await ensureIsNonExistingCoreName(name, this.corestore)

    const core = this.corestore.get({ ...opts, name })
    await core.ready()

    return core
  }

  async createBee (name, opts) {
    const core = await this.createCore(name)

    const bee = new Hyperbee(core, { ...this.defaultBeeOpts, ...opts })
    await bee.ready()

    return bee
  }

  async readBee (key, opts = {}) {
    const name = opts.name
    const core = await this.readCore(key, { name })

    const bee = new Hyperbee(core, { ...this.defaultBeeOpts, ...opts })

    await bee.ready()
    return bee
  }

  async readCore (key, opts) {
    const core = this.corestore.get({ ...opts, key: asBuffer(key) })
    await core.ready()
    return core
  }

  async readCores (keys, opts) {
    const readPromises = keys.map((key) => this.readCore(key, opts))
    return await Promise.all(readPromises)
  }

  async getEntry ({ hash, location, version = undefined, ...opts }) {
    const fullBee = await this.readBee(hash, opts)
    const bee = version == null ? fullBee.snapshot() : fullBee.checkout(version)

    const entry = await bee.get(location)
    if (entry === null) {
      throw new Error(
        `No entry at '${location}' for bee at ${asHex(hash)} (version ${version})`
      )
    }

    return entry.value
  }

  async ready () {
    await this.corestore.ready()
  }

  async close () {
    await this.corestore.close()
  }
}

module.exports = HyperInterface
