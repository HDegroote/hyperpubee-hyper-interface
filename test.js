const { strict: nodeAssert } = require('assert')
const { expect } = require('chai')
const Corestore = require('corestore')
const ram = require('random-access-memory')
const b4a = require('b4a')

const HyperInterface = require('.')

async function hyperInterfaceFactory () {
  const corestore = new Corestore(ram)

  const hyperInterface = new HyperInterface(corestore)
  await hyperInterface.ready()

  return hyperInterface
}

describe('Hypercore-interface tests', function () {
  let hyperInterface
  const textOpts = { keyEncoding: 'utf-8', valueEncoding: 'utf-8' }
  Object.freeze(textOpts)

  this.beforeEach(async function () {
    hyperInterface = await hyperInterfaceFactory()
  })

  this.afterEach(async function () {
    await hyperInterface.close()
  })

  describe('Tests with bee', function () {
    let bee
    const name = 'testbee'
    const myKey = 'myKey'

    this.beforeEach(async function () {
      bee = await hyperInterface.createBee(name)
      await bee.put(myKey, 'myValue')
    })

    it('Can create and read a hyperbee', async function () {
      const bee2 = await hyperInterface.readBee(bee.feed.key, textOpts)

      const { value: actualValue } = await bee2.get('myKey')
      expect(actualValue).to.equal('myValue')
    })

    it('Can get an entry', async function () {
      const entry = await hyperInterface.getEntry({ hash: bee.feed.key, location: 'myKey', ...textOpts })
      expect(entry).to.equal('myValue')
    })

    it('Can get an old entry', async function () {
      const initV = bee.version
      await bee.put(myKey, 'new value')

      const newEntry = await hyperInterface.getEntry({ hash: bee.feed.key, location: 'myKey', ...textOpts })
      const oldEntry = await hyperInterface.getEntry(
        { hash: bee.feed.key, location: 'myKey', version: initV, ...textOpts }
      )

      expect(newEntry).to.equal('new value')
      expect(oldEntry).to.equal('myValue')
    })

    it('Throws when an entry is not found', async function () {
      await nodeAssert.rejects(
        hyperInterface.getEntry({ hash: bee.feed.key, location: 'nothingHere' }),
        { message: /No entry at '.+/ }
      )
    })

    it('Can read multiple hypercores', async function () {
      const core = await hyperInterface.createCore('some core')
      const cores = await hyperInterface.readCores([core.key, bee.feed.key])
      expect(cores.map((c) => c.key)).to.deep.equal([core.key, bee.feed.key])
    })
  })

  it('throws when attempting to create a hyperbee with an already-taken name', async function () {
    const name = 'duplicateName'
    const bee = await hyperInterface.createBee(name)
    await bee.put('no longer', 'empty')

    await nodeAssert.rejects(hyperInterface.createBee(name), {
      message: "A hypercore with name 'duplicateName' already exists in the corestore"
    })
  })

  it('throws when attempting to create a hyperbee when one with the same name is already used for empty hyperbee', async function () {
    const name = 'duplicateName'
    await hyperInterface.createBee(name)

    await nodeAssert.rejects(hyperInterface.createBee(name), {
      message: "A hypercore with name 'duplicateName' already exists in the corestore"
    })
  })

  it('handles default bee opts correctly', async function () {
    const defaultBeeOpts = { keyEncoding: 'utf-8', valueEncoding: 'json' }
    const interf = new HyperInterface(hyperInterface.corestore, { defaultBeeOpts })

    const bee = await interf.createBee('my bee')
    await bee.put('hey', { ho: 'la' })
    expect((await bee.get('hey')).value).to.deep.equal({ ho: 'la' })

    const readBee = await interf.readBee(bee.feed.key)
    expect((await readBee.get('hey')).value).to.deep.equal({ ho: 'la' })

    const res = await interf.getEntry(
      { hash: bee.feed.key, location: b4a.from('hey'), keyEncoding: 'binary' }
    )
    expect(res).to.deep.equal({ ho: 'la' })
  })

  it('Can read a core by name', async function () {
    const core = await hyperInterface.createCore('core')
    await core.append('block')

    const readCore = await hyperInterface.readCore(null, { name: 'core', valueEncoding: 'utf-8' })
    expect(await readCore.get(0)).to.equal('block')
  })

  it('Can read a bee by name', async function () {
    const bee = await hyperInterface.createBee('bee', textOpts)
    await bee.put('block', 'ok')

    const readBee = await hyperInterface.readBee(null, { name: 'bee', ...textOpts })
    expect((await readBee.get('block')).value).to.equal('ok')
  })
})
